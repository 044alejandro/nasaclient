//
//  RoverTarget.swift
//  NASAClient
//
//  Created by Alejandra Rusakovskaya on 09.09.2022.
//

import Foundation
import Moya


enum RoverTarget {
    case getPhotos(page: Int, sol: Int?, date: Date?, camera: CameraEnum?)
}

extension RoverTarget: TargetType{
    var baseURL: URL {
        return URL(string: "https://api.nasa.gov/mars-photos/")!
    }
    
    var path: String {
        return "api/v1/rovers/curiosity/photos"
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        var parameters: [String: Any] = [:]
        switch self {
        case .getPhotos(let page, let sol, let date, let camera):
            parameters[NASAParameters.api_key.rawValue] = DataRequest.key2.rawValue
            parameters[NASAParameters.page.rawValue] = page
            parameters[NASAParameters.camera.rawValue] = camera
            parameters[NASAParameters.sol.rawValue] = sol
            if let requestDate = date {
                parameters[NASAParameters.earth_date.rawValue] = FillData().fillDate(date: requestDate)
            }
        }
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
    
    var headers: [String : String]? {
        return nil
    }
}

