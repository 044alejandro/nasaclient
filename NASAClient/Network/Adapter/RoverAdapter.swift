//
//  RoverAdapter.swift
//  NASAClient
//
//  Created by Alejandra Rusakovskaya on 09.09.2022.
//

import Foundation
import Moya
import PromiseKit

struct RoverAdapter{
    let provider = MoyaProvider<RoverTarget>()
    
    func getPhotos(page: Int, sol: Int?, date: Date?, camera: CameraEnum?) -> Promise<Photos> {
        return Promise { steal in
            provider.request(.getPhotos(page: page, sol: sol, date: date, camera: camera), completion: { result in
                switch result{
                case .success(let response):
                    do{
                        let photos = try JSONDecoder().decode(Photos.self, from: response.data)
                        steal.fulfill(photos)
                    } catch let error as NSError {
                        print( error.localizedDescription)
                        steal.reject(error)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    steal.reject(error)
                }
            })
        }
    }
}


