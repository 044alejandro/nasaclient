//
//  RestData.swift
//  NASAClient
//
//  Created by Виктор Сирик on 21.04.2022.
//

import Foundation

class RestData{
    
    func loadModel<T: Codable>(urlPath: String, parameters: [String: Any]?, complectionHandler: @escaping(T?, String?) -> Void) {
        NetworkManager.shared.loadData(urlPath: urlPath, method: .get, parameters: parameters){ data, error in
            guard error == nil else {
                complectionHandler(nil, error)
                return
            }
            guard let data = data else {
                complectionHandler(nil, nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(T.self, from: data)
                complectionHandler(response, nil)
            } catch let errorRes as NSError {
                complectionHandler(nil, errorRes.localizedDescription)
            }
        }
    }
}
