//
//  FillData.swift
//  NASAClient
//
//  Created by Виктор Сирик on 23.04.2022.
//

import Foundation

class FillData {
    //fill array with cameras
    func fillCameraArray() -> [CameraEnum] {
        let cameras: [CameraEnum] = CameraEnum.allCases
        return cameras
    }
    
    //fill array with rovers
    func fillRoverArray() -> [RoverEnum] {
        let modelRoverArray: [RoverEnum]  = RoverEnum.allCases
        return modelRoverArray
    }
    
    func fillDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
     }
}
