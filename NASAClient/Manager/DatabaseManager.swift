//
//  DatabaseManager.swift
//  NASAClient
//
//  Created by Виктор Сирик on 22.04.2022.
//

import Foundation
import RealmSwift

class DatabaseManager {
    
    static let shared = DatabaseManager()
    let realm = try? Realm()
    
    // save data to realm
    func saveData(data: [History]) {
        let transformToDb = transformToDatabase(data: data)
        do {
            try realm?.write({
                realm?.add(transformToDb)
            })
        } catch(let error){
            print(error.localizedDescription)
        }
    }
    
    //transform data from api model to database model
    private func transformToDatabase(data: [History]) -> HistoryListDatabase {
        let listOfHistoryDatabase: List<HistoryDatabase> = List<HistoryDatabase>()
        let historyModelDatabase = HistoryListDatabase()
        
        for item in data {
            let itemData = HistoryDatabase()
            let rovers: List<String> = List<String>()
            itemData.sol = item.sol
            itemData.date = item.date
            itemData.camera = item.camera?.rawValue
            for rover in item.rover ?? [] {
                rovers.append("\(rover)")
            }
            itemData.rover = rovers
            listOfHistoryDatabase.append(itemData)
        }
        historyModelDatabase.historyList = listOfHistoryDatabase
        return historyModelDatabase
    }
    
    //transform data from database model to api model
    private func transformDataModel(data: HistoryListDatabase) -> [History] {
        var listOfHistory: [History] = []
        for item in data.historyList {
            var itemData = History()
            var rovers: [RoverEnum] = []
            itemData.sol = item.sol
            itemData.date = item.date
            itemData.camera = CameraEnum(rawValue: item.camera ?? "")
            rovers = item.rover.compactMap{ item in
                RoverEnum(rawValue: item)
            }
            itemData.rover = rovers
            listOfHistory.append(itemData)
        }
        return listOfHistory
    }
    
    //get data from realm
    func getData() -> [History]? {
        guard let history = realm?.objects(HistoryListDatabase.self).last else { return nil }
        return transformDataModel(data: history)
    }
}
