//
//  AlertManager.swift
//  NASAClient
//
//  Created by Виктор Сирик on 22.04.2022.
//

import Foundation
import UIKit

class AlertManager {
    // show alert if not chosen filters
    func selectData() -> UIAlertController {
        let alert = UIAlertController(title: "Error", message: "Select sol or date", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        return alert
    }
}
