//
//  ViewController + Extensions.swift
//  NASAClient
//
//  Created by Виктор Сирик on 21.04.2022.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: photosCell, for: indexPath) as? PhotosCell {
            cell.update(data: photos[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2) - 5, height: collectionView.frame.height / 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let photoVC = storyboard?.instantiateViewController(withIdentifier: photoVC) as? PhotoViewController{
        photoVC.photo = photos[indexPath.row]
        self.navigationController?.pushViewController(photoVC, animated: true)
        }
    }
    
    //update collectionView on scroll
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == photos.count - 1{
            paginationPage += 1
            loadPhotos()
        } 
    }
}

//update collectionView when data reterned
extension ViewController: FilterViewControllerDelegate{
    func selectDataFilter(history: History) {
        notFoundView.removeFromSuperview()
        paginationPage = 1
        requestData = history
        photos = []
        loadPhotos()
    }
}

