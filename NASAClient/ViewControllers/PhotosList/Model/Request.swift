//
//  Request.swift
//  NASAClient
//
//  Created by Alejandra Rusakovskaya on 09.05.2022.
//

import Foundation

enum NASAParameters: String {
    case page
    case api_key
    case earth_date
    case camera
    case sol
}

enum DataRequest: String{
    case key = "O3WoMXfYW1cofffEYguGyLfNylca0eFCctWm6K8R"
    case key2 = "yHMwZm8W4YGNPPSRf2Kq1h2e1LjwSY5OVHzbffZr"
    case url = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos"
}
