//
//  Photos.swift
//  NASAClient
//
//  Created by Виктор Сирик on 21.04.2022.
//

import Foundation

// MARK: - Photos
struct Photos: Codable {
    var photos: [Photo]?
}

// MARK: - Photo
struct Photo: Codable {
    var id, sol: Int?
    var camera: Camera?
    var img_src: String?
    var earth_date: String?
    var rover: Rover?
}

// MARK: - Camera
struct Camera: Codable {
    var id: Int?
    var name: String?
    var rover_id: Int?
    var full_name: String?
}

// MARK: - Rover
struct Rover: Codable {
    var id: Int?
    var name, landing_date, launch_date, status: String?
}

