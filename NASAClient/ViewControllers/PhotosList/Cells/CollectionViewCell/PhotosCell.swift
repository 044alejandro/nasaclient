//
//  PhotosCell.swift
//  NASAClient
//
//  Created by Виктор Сирик on 21.04.2022.
//

import UIKit
import Kingfisher
class PhotosCell: UICollectionViewCell {

    @IBOutlet weak var photoImage: UIImageView!
    let viewShadow = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func update (data: Photo) {
        loadImage(url: data.img_src ?? "")
    }
 
    func loadImage(url: String) {
        let urlPath = URL(string: url)
        photoImage.kf.setImage(with: urlPath)
    }
}
