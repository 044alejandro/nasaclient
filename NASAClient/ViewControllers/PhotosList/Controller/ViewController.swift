//
//  ViewController.swift
//  NASAClient
//
//  Created by Виктор Сирик on 21.04.2022.
//
//


import UIKit
import SnapKit
import PromiseKit

class ViewController: UIViewController {
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var photos: [Photo] = []
    var paginationPage = 1
    var requestData = History()
    let notFoundView = UIView()
    
    let photosCell = "PhotosCell"
    let historyViewController = "HistoryViewController"
    let filterViewController = "FilterViewController"
    let photoVC = "PhotoViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBarItems()
        photosCollectionView.delegate = self
        photosCollectionView.register(UINib(nibName: photosCell, bundle: nil), forCellWithReuseIdentifier: photosCell)
        loadPhotos()
    }
    
    //add Navigation Bar items
    func addNavBarItems() {
        var historyButton = UIBarButtonItem()
        var filterButton = UIBarButtonItem()
        if #available(iOS 13.0, *) {
            historyButton = UIBarButtonItem(image: UIImage(systemName: "list.bullet"), style: .done, target: self, action: #selector(showHistoryAction))
            filterButton = UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .done, target: self, action: #selector(showFilterAction))
        } else {
            historyButton = UIBarButtonItem(title: "History", style: .done, target: self, action: #selector(showHistoryAction))
            filterButton = UIBarButtonItem(title: "Filter", style: .done, target: self, action: #selector(showFilterAction))
        }
        self.navigationItem.title = "Results"
        self.navigationItem.rightBarButtonItem = historyButton
        self.navigationItem.leftBarButtonItem = filterButton
    }
    
    @objc func showHistoryAction(){
        if let historyVC = self.storyboard?.instantiateViewController(withIdentifier: historyViewController) as? HistoryViewController {
            historyVC.delegate = self
        self.navigationController?.pushViewController(historyVC, animated: true)
        }
    }
    
    @objc func showFilterAction(){
        if let filterVC = self.storyboard?.instantiateViewController(withIdentifier: filterViewController) as? FilterViewController{
            filterVC.delegate = self
        present(filterVC, animated: true, completion: nil)
        }
    }
    
    //add view if array with photos is empty
    func addViewSearchMore() {
        let searchMoreLabel = UILabel()
        let filterButton = UIButton()
        searchMoreLabel.text = "Not found results"
        searchMoreLabel.textColor = .gray
        notFoundView.addSubview(searchMoreLabel)
        filterButton.setTitle("Filter", for: .normal)
        filterButton.setTitleColor(.white, for: .normal)
        filterButton.backgroundColor = .systemBlue
        filterButton.addTarget(self, action: #selector(showFilterAction), for: .touchUpInside)
        notFoundView.addSubview(filterButton)
        view.addSubview(notFoundView)
        
        notFoundView.snp.makeConstraints{ maker in
            maker.height.equalTo(200)
            maker.width.equalTo(200)
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview()
        }
        searchMoreLabel.snp.makeConstraints{ maker in
            maker.height.equalTo(20)
            maker.width.equalTo(140)
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview()
        }
        filterButton.snp.makeConstraints{ maker in
            maker.height.equalTo(40)
            maker.width.equalTo(120)
            maker.centerX.equalToSuperview()
            maker.top.equalTo(searchMoreLabel.snp.top).inset(30)
        }
    }
    
    //load data
    func loadPhotos() {
        firstly{
            RoverAdapter().getPhotos(page: paginationPage, sol: requestData.sol, date: requestData.date, camera: requestData.camera)
        }.done { response in
            guard let data = response.photos else { return }
            
            self.photos += data
            self.filterRovers()
            if self.photos.isEmpty {
                self.addViewSearchMore()
            } else {self.notFoundView.removeFromSuperview()}
            self.photosCollectionView.reloadData()
        } .catch { error in
            print(error.localizedDescription)
        }
    }
    
    //filter rovers
    func filterRovers(){
        if !requestData.rover.isEmpty{
            var photosFiltered: [Photo] = []
            for item in requestData.rover {
                photosFiltered += photos.filter{ $0.rover?.name == "\(item)" }
            }
            self.photos = photosFiltered
        }
    }
}

