//
//  FilterViewController.swift
//  NASAClient
//
//  Created by Виктор Сирик on 23.04.2022.
//

import UIKit
import SnapKit

protocol FilterViewControllerDelegate {
    func selectDataFilter(history: History)
}

class FilterViewController: UIViewController {

    @IBOutlet weak var camerasTableView: UITableView!
    
    var cameras: [CameraEnum] = []
    var selectFilterSegmentControl = UISegmentedControl()
    var dateTextField = UITextField()
    var solTextField = UITextField()
    var datePickerView = UIDatePicker()
    var dataFilterLabel = UILabel()
    var rovers: [RoverEnum] = []
    var isSelectedFirstSection = true
    var delegate: FilterViewControllerDelegate?
    var history: [History] = []
    let filterCell = "FilterCell"
    let historyCell = "HistoryCell"

    
    var selectedRovers: [RoverEnum] = []
    var selectedCAmera: CameraEnum?

    override func viewDidLoad() {
        super.viewDidLoad()
        history = DatabaseManager.shared.getData() ?? []
        cameras = FillData().fillCameraArray()
        rovers = FillData().fillRoverArray()
        hideKeyboardWhenTappedAround()
        camerasTableView.reloadData()
        camerasTableView.register(UINib(nibName: filterCell, bundle: nil), forCellReuseIdentifier: filterCell)
        camerasTableView.register(HistoryCell.self, forCellReuseIdentifier: historyCell)
        addSegmentControl()
        addLabel()
        addDatePicker()
    }
    
    // add UISegmentedControl to select date or sol
    func addSegmentControl() {
        selectFilterSegmentControl = UISegmentedControl(items: [ SegmentSections.date.rawValue, SegmentSections.sol.rawValue])
        selectFilterSegmentControl.selectedSegmentIndex = 0
        selectFilterSegmentControl.addTarget(self, action: #selector(selectTypeFilter(_:)), for: .valueChanged)
        view.addSubview(selectFilterSegmentControl)
        selectFilterSegmentControl.translatesAutoresizingMaskIntoConstraints = false
        selectFilterSegmentControl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        selectFilterSegmentControl.widthAnchor.constraint(equalToConstant: 300).isActive = true
        selectFilterSegmentControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 25).isActive = true
        selectFilterSegmentControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    //action to select segments in UISegmentedControl
    @objc func selectTypeFilter(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            solTextField.removeFromSuperview()
            addDatePicker()
            isSelectedFirstSection = true
        default:
            dateTextField.removeFromSuperview()
            addSolTextField()
            isSelectedFirstSection = false
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    func addLabel() {
        view.addSubview(dataFilterLabel)
        dataFilterLabel.translatesAutoresizingMaskIntoConstraints = false
        dataFilterLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        dataFilterLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        dataFilterLabel.topAnchor.constraint(equalTo: selectFilterSegmentControl.bottomAnchor, constant: 30).isActive = true
        dataFilterLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        camerasTableView.topAnchor.constraint(equalTo: dataFilterLabel.bottomAnchor, constant: 0).isActive = true
    }
    
    // add UIDatePicker to select date and max date in picker
    func addDatePicker() {
        dateTextField.inputView = datePickerView
        view.addSubview(dateTextField)
        dataFilterLabel.text = "Date"
        dateTextField.layer.borderWidth = 1
        dateTextField.layer.borderColor = UIColor.black.cgColor
        dateTextField.layer.cornerRadius = 3
        
        dateTextField.translatesAutoresizingMaskIntoConstraints = false
        dateTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        dateTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        dateTextField.topAnchor.constraint(equalTo: selectFilterSegmentControl.bottomAnchor, constant: 30).isActive = true
        dateTextField.leftAnchor.constraint(equalTo: dataFilterLabel.rightAnchor, constant: 10).isActive = true
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: UIControl.Event.valueChanged)
        if #available(iOS 13.4, *) {
            datePickerView.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } 
        datePickerView.datePickerMode = .date
        
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
            var components = DateComponents()
            components.calendar = calendar
            components.year = 0
            let maxDate = calendar.date(byAdding: components, to: currentDate)
            datePickerView.maximumDate = maxDate
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)

        toolBar.setItems([doneButton, flexSpace], animated: true)
        dateTextField.inputAccessoryView = toolBar
        dateTextField.placeholder = " Select date"
    }
    
    // formatting date
   @objc func datePickerValueChanged(sender: UIDatePicker) {
       dateTextField.text = FillData().fillDate(date: sender.date)
    }
    
    // add UITextField to select number of sol
    func addSolTextField() {
        view.addSubview(solTextField)
        dataFilterLabel.text = "Sol"
        solTextField.keyboardType = .numberPad
        solTextField.layer.borderWidth = 1
        solTextField.layer.borderColor = UIColor.black.cgColor
        solTextField.layer.cornerRadius = 3
        solTextField.translatesAutoresizingMaskIntoConstraints = false
        solTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        solTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        solTextField.topAnchor.constraint(equalTo: selectFilterSegmentControl.bottomAnchor, constant: 30).isActive = true
        solTextField.leftAnchor.constraint(equalTo: dataFilterLabel.rightAnchor, constant: 10).isActive = true
        solTextField.placeholder = " Enter number of sol"
    }
    
    @objc func doneAction() {
        view.endEditing(true)
    }
    
    // action to save data and dismiss VC
    @IBAction func selectFilterAction(_ sender: Any) {
        var historyResult = History()
        historyResult.camera = selectedCAmera
        historyResult.rover = selectedRovers
        if let number = solTextField.text, !number.isEmpty && !isSelectedFirstSection{
            historyResult.sol = Int(number) ?? nil
        } else if isSelectedFirstSection && dateTextField.text != ""{
            historyResult.date = datePickerView.date
        }
        if historyResult.sol == nil && historyResult.date == nil {
            let alert = AlertManager().selectData()
            present(alert, animated: true, completion: nil)
        } else {
            var historyDatabase = DatabaseManager.shared.getData() ?? []
            historyDatabase.append(historyResult)
            DatabaseManager.shared.saveData(data: historyDatabase)
            delegate?.selectDataFilter(history: historyResult)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
