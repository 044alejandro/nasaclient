//
//  FilterCell.swift
//  NASAClient
//
//  Created by Виктор Сирик on 23.04.2022.
//

import UIKit

protocol FilterCellDelegate {
    func selectItem(index: Int, tableView: Int, chechBoxIsHidden: Bool)
}

class FilterCell: UITableViewCell {

    @IBOutlet weak var cameraTypeLabel: UILabel!
    @IBOutlet weak var checkBoxRoundView: UIView!
    @IBOutlet weak var checkBoxSelectView: UIView!
    
    var delegate: FilterCellDelegate?
    var tableView = 0
    
    func createSelectedButton() {
        checkBoxRoundView.layer.borderWidth = 1
        checkBoxRoundView.layer.borderColor = UIColor.black.cgColor
        checkBoxRoundView.layer.cornerRadius = checkBoxRoundView.frame.height / 2
        checkBoxSelectView.layer.cornerRadius = checkBoxSelectView.frame.height / 2
    }

    func updateCameraCell(rover: RoverEnum?, camera: CameraEnum?, tableView: Int, isSelectedCell: Bool) {
        createSelectedButton()
        self.tableView = tableView
        checkBoxSelectView.isHidden = !isSelectedCell
        if let roverRes = rover, camera == nil {
            cameraTypeLabel.text = "\(roverRes)"
        } else if rover == nil {
            cameraTypeLabel.text = camera?.rawValue
        }
    }
    
    @IBAction func selectCameraAction(_ sender: Any) {
        delegate?.selectItem(index: tag, tableView: tableView, chechBoxIsHidden: checkBoxSelectView.isHidden)
    }
}
