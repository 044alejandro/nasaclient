//
//  Camera.swift
//  NASAClient
//
//  Created by Виктор Сирик on 22.04.2022.
//

import Foundation
enum CameraEnum: String, CaseIterable {
    case FHAZ = "Front Hazard Avoidance Camera"
    case RHAZ = "Rear Hazard Avoidance Camera"
    case MAST = "Mast Camera"
    case CHEMCAM = "Chemistry and Camera Complex"
    case MAHLI = "Mars Hand Lens Imager"
    case MARDI = "Mars Descent Imager"
    case NAVCAM = "Navigation Camera"
    case PANCAM = "Panoramic Camera"
    case MINITES = "Miniature Thermal Emission Spectrometer (Mini-TES)"
}

enum RoverEnum : String, CaseIterable{
    case Curiosity
    case Opportunity
    case Spirit
}

enum SegmentSections: String{
    case date = "Date"
    case sol = "Sol"
    
}




