//
//  FilterViewController + Extensions.swift
//  NASAClient
//
//  Created by Виктор Сирик on 23.04.2022.
//

import Foundation
import UIKit

extension FilterViewController: UITableViewDelegate, UITableViewDataSource, FilterCellDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if history.count > 3 {
                let count = history.count - 3
                history = history.enumerated().filter{$0.offset >= count}.map{ $0.element }
            }
            return history.count
        case 1: return rovers.count
        default: return cameras.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            if history.count == 0 { return "" } else { return "Last history" }
        case 1: return "Select rover"
        default: return "Select camera type"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 1...2: return UITableView.automaticDimension
        default: return 70
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: historyCell, for: indexPath) as? HistoryCell{
                cell.update(history: history[indexPath.row])
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: filterCell, for: indexPath) as? FilterCell{
                cell.tag = indexPath.row
                
                let filterRovers = selectedRovers.filter({$0 == rovers[indexPath.row]})
                let isSelected = !filterRovers.isEmpty ? true : false
                cell.updateCameraCell(rover: rovers[indexPath.row], camera: nil, tableView: indexPath.section, isSelectedCell: isSelected)
                cell.delegate = self
                return cell
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: filterCell, for: indexPath) as? FilterCell{
                cell.tag = indexPath.row
                let isSelected = selectedCAmera == cameras[indexPath.row]
                cell.updateCameraCell(rover: nil, camera: cameras[indexPath.row], tableView: indexPath.section , isSelectedCell: isSelected)
                cell.delegate = self
                return cell
            }
        default: return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            tableView.deselectRow(at: indexPath, animated: true)
            let historyResult = History(sol: history[indexPath.row].sol, date: history[indexPath.row].date, camera: history[indexPath.row].camera, rover: history[indexPath.row].rover)
            delegate?.selectDataFilter(history: historyResult)
            dismiss(animated: true, completion: nil)
        }
    }
    
    // selected camera
    func selectItem(index: Int, tableView: Int, chechBoxIsHidden: Bool) {
        switch tableView {
        case 1:
            if chechBoxIsHidden {
                selectedRovers.append(rovers[index])
            } else {
                selectedRovers.removeAll(where:{ $0 == rovers[index]})
            }
        default:
            if chechBoxIsHidden {
                selectedCAmera = cameras[index]
            } else{
                selectedCAmera = nil
            }
        }
        camerasTableView.reloadData()
    }
}

