//
//  PhotoViewController.swift
//  NASAClient
//
//  Created by Виктор Сирик on 22.04.2022.
//

import UIKit
import Kingfisher

class PhotoViewController: UIViewController {
    var imageScrollView = ImageScrollView()
    var imageView = UIImageView()
    var photo = Photo(camera: Camera(), rover: Rover())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageScrollView = ImageScrollView(frame: view.bounds)
        view.addSubview(imageScrollView)
        setUpImageScrollView()
        loadImage(url: photo.img_src ?? "")
        guard let image = imageView.image else {return}
        imageScrollView.setImage(image: image)
    }
    
    //set image
    func setUpImageScrollView() {
        imageScrollView.translatesAutoresizingMaskIntoConstraints = false
        imageScrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imageScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        imageScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        imageScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    //load image
    func loadImage(url: String) {
        let urlPath = URL(string: url)
        imageView.kf.setImage(with: urlPath)
    }
}
