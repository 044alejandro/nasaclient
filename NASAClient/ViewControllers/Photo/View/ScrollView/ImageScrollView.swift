//
//  ImageScrollView.swift
//  NASAClient
//
//  Created by Виктор Сирик on 28.04.2022.
//

import UIKit

class ImageScrollView: UIScrollView {

    var imageScrollView = UIImageView()
    lazy var zoomingTap: UITapGestureRecognizer = {
        let zoomingTap = UITapGestureRecognizer(target: self, action: #selector(tapToZoomAction))
        zoomingTap.numberOfTapsRequired = 2
        return zoomingTap
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delegate = self
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        self.decelerationRate = UIScrollView.DecelerationRate.normal
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        centerImage()
    }

    // tap gesture for zooming image
    @objc func tapToZoomAction(sender: UITapGestureRecognizer) {
        let location = sender.location(in: sender.view)
        self.zoomPic(point: location, animated: true)
    }

    func zoomPic(point: CGPoint, animated: Bool) {
        let currentScale = self.zoomScale
        let minScale = minimumZoomScale
        let maxScale = maximumZoomScale

        if (minScale == maxScale && minScale > 1) {
            return
        }
        let toScale = maxScale
        let finalScale = (currentScale == minScale) ? toScale : minScale
        let zoomRect = zoomRect(scale: finalScale, center: point)
        self.zoom(to: zoomRect, animated: animated)
    }

    func zoomRect(scale: CGFloat, center: CGPoint) -> CGRect{
        var zoomRect = CGRect.zero
        let bounds = self.bounds

        zoomRect.size.width = bounds.size.width / scale
        zoomRect.size.height = bounds.size.height / scale
        zoomRect.origin.x = center.x - (zoomRect.size.width / 2)
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2)
        return zoomRect
    }
    
    //set image
    func setImage(image: UIImage) {
        imageScrollView.removeFromSuperview()
        imageScrollView = UIImageView(image: image)
        self.addSubview(imageScrollView)
        configurationFor(imageSize: image.size)
    }

    func configurationFor(imageSize: CGSize) {
        self.contentSize = imageSize

        setMaxMinZoomScale()
        self.zoomScale = minimumZoomScale
        imageScrollView.isUserInteractionEnabled = true
        imageScrollView.addGestureRecognizer(zoomingTap)
    }

    func centerImage() {
        let boundsSize = self.bounds.size
        var frameToCenter = imageScrollView.frame
        if frameToCenter.size.width < boundsSize.width {
            frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2
        } else {
            frameToCenter.origin.x = 0
        }

        if frameToCenter.size.height < boundsSize.height {
            frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 3
        } else {
            frameToCenter.origin.y = 0
        }
        imageScrollView.frame = frameToCenter
    }

    //resize image
    func setMaxMinZoomScale() {
        let boundsSize = self.bounds.size
        let imageSize = imageScrollView.bounds.size

        let xScale = boundsSize.width / imageSize.width
        let yScale = boundsSize.height / imageSize.height
        let minScale = min(xScale, yScale)

        var maxScale: CGFloat = 1.0
        if minScale < 0.1 {
            maxScale = 0.3
        }
        if minScale >= 0.1 && minScale < 0.5 {
            maxScale = 0.7
        }
        if minScale >= 0.5 {
            maxScale = max(1.0, minScale)
        }

        minimumZoomScale = minScale
        maximumZoomScale = maxScale
    }
}

extension ImageScrollView: UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageScrollView
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerImage()
    }
}
