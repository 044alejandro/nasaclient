//
//  HistoryViewController.swift
//  NASAClient
//
//  Created by Виктор Сирик on 22.04.2022.
//

import UIKit
import SnapKit

class HistoryViewController: UIViewController {
    
    var historyTableView = UITableView()
    var history: [History] = []
    var delegate: FilterViewControllerDelegate?
    let historyCell = "HistoryCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        history = DatabaseManager.shared.getData() ?? []
        setTableView()
    }
    
    func setTableView() {
        historyTableView.dataSource = self
        historyTableView.delegate = self
        historyTableView.reloadData()
        view.addSubview(historyTableView)

        historyTableView.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(15)
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
            maker.bottom.equalToSuperview().inset(0)
        }
        historyTableView.register(HistoryCell.self, forCellReuseIdentifier: historyCell)
    }
}
