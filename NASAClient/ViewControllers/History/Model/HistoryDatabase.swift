//
//  HistoryDatabase.swift
//  NASAClient
//
//  Created by Виктор Сирик on 23.04.2022.
//

import Foundation
import RealmSwift

class HistoryListDatabase: Object {
    @Persisted var historyList: List<HistoryDatabase>
}

class HistoryDatabase: Object {
   @Persisted var sol: Int?
   @Persisted var date: Date?
   @Persisted var camera: String?
   @Persisted var rover: List<String>

}


