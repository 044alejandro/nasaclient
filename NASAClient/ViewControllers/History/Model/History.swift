//
//  History.swift
//  NASAClient
//
//  Created by Виктор Сирик on 22.04.2022.
//

import Foundation

struct History {
    var sol: Int?
    var date: Date?
    var camera: CameraEnum?
    var rover: [RoverEnum] = []
}
