//
//  HistoryViewController + Extensions.swift
//  NASAClient
//
//  Created by Виктор Сирик on 22.04.2022.
//

import Foundation
import UIKit

extension HistoryViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: historyCell, for: indexPath) as? HistoryCell {
        cell.update(history: history[indexPath.row])
        return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    //delete item from history
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            history.remove(at: indexPath.row)
            DatabaseManager.shared.saveData(data: history)
        }
        tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
    // search data from history
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let historyResult = History(sol: history[indexPath.row].sol, date: history[indexPath.row].date, camera: history[indexPath.row].camera, rover: history[indexPath.row].rover) 
        delegate?.selectDataFilter(history: historyResult)
        navigationController?.popViewController(animated: true)
    }
}
