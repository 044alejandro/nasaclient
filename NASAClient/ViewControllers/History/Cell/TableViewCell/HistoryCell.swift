//
//  HistoryCell.swift
//  NASAClient
//
//  Created by Виктор Сирик on 22.04.2022.
//

import UIKit
import SnapKit

class HistoryCell: UITableViewCell {
    var cameraLabel = UILabel()
    var secondItemLabel = UILabel()
    var roverLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func setupView() {
        contentView.addSubview(cameraLabel)
        contentView.addSubview(secondItemLabel)
        contentView.addSubview(roverLabel)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        cameraLabel.snp.makeConstraints{ maker in
            maker.top.equalToSuperview().offset(10)
            maker.left.equalToSuperview().offset(20)
        }
        
        roverLabel.snp.makeConstraints{ maker in
            maker.top.equalTo(cameraLabel.snp.bottom).inset(-10)
            maker.left.equalToSuperview().offset(20)
        }
        
        secondItemLabel.snp.makeConstraints{ maker in
            maker.top.equalToSuperview().offset(10)
            maker.left.equalTo(cameraLabel.snp.right).inset(-20)
        }
    }
    
    func update(history: History) {
        //!!!!!!!!!
        cameraLabel.text = history.camera != nil ? "Camera: \(history.camera!)" : "Camera: not selected"
        if let date = history.date {
            secondItemLabel.text = "Date: \(FillData().fillDate(date: date))"
        } else if let sol = history.sol {
            secondItemLabel.text = "Sol: \(sol)"
        }
        let rovers = history.rover.map({$0.rawValue})
        roverLabel.text = !history.rover.isEmpty ? "Rovers: \(rovers.joined(separator: ", ")) " : "Rovers: not selected"
    }
}
